<?php


namespace App\Http\Controllers;

use App\Services\OrganizationService;

/**
 * 组织机构-控制器
 * @author 牧羊人
 * @since 2021/7/12
 * Class OrganizationController
 * @package App\Http\Controllers
 */
class OrganizationController extends Backend
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2021/7/12
     * OrganizationController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new OrganizationService();
    }
}
